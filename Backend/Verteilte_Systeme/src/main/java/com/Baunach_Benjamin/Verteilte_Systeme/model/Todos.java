package com.Baunach_Benjamin.Verteilte_Systeme.model;

import jakarta.persistence.*; // for Spring Boot 3

@Entity
@Table(name = "todos")
public class Todos {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id; 

    @Column(name = "title")
    private String title; 

    @Column(name = "description")
    private String description;

    public Todos() {

    }

    public Todos(String title, String description) {
        this.title = title;
        this.description = description;  
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString(){
        return "Todos [id =" + id + ", title=" + title + ", description=" + description + "]";
    }
}
