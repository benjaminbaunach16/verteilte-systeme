package com.Baunach_Benjamin.Verteilte_Systeme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VerteilteSystemeApplication {

	public static void main(String[] args) {
		SpringApplication.run(VerteilteSystemeApplication.class, args);
	}

}
