package com.Baunach_Benjamin.Verteilte_Systeme.service;

import com.Baunach_Benjamin.Verteilte_Systeme.model.Todos;
import com.Baunach_Benjamin.Verteilte_Systeme.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TodoService {

    @Autowired
    private TodoRepository todoRepository;

    public List<Todos> getAllTodos(String title) {
        List<Todos> todos = new ArrayList<>();

        if (title == null)
            todoRepository.findAll().forEach(todos::add);
        else
            todoRepository.findByTitleContaining(title).forEach(todos::add);

        return todos;
    }

    public Optional<Todos> getTodoById(long id) {
        return todoRepository.findById(id);
    }

    public Todos createTodo(Todos todo) {
        return todoRepository.save(new Todos(todo.getTitle(), todo.getDescription()));
    }

    public Optional<Todos> updateTodo(long id, Todos todo) {
        return todoRepository.findById(id)
                .map(existingTodo -> {
                    existingTodo.setTitle(todo.getTitle());
                    existingTodo.setDescription(todo.getDescription());
                    return todoRepository.save(existingTodo);
                });
    }

    public void deleteTodo(long id) {
        todoRepository.deleteById(id);
    }

    public void deleteAllTodos() {
        todoRepository.deleteAll();
    }
}