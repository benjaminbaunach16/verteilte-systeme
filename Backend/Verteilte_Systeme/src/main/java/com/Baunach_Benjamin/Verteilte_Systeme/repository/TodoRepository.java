package com.Baunach_Benjamin.Verteilte_Systeme.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.Baunach_Benjamin.Verteilte_Systeme.model.Todos;

public interface TodoRepository extends JpaRepository<Todos, Long> {

    List<Todos> findByTitleContaining(String title);

}

