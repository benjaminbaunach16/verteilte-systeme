package com.Baunach_Benjamin.Verteilte_Systeme.controller;

import com.Baunach_Benjamin.Verteilte_Systeme.model.Todos;
import com.Baunach_Benjamin.Verteilte_Systeme.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class TodosController {

    @Autowired
    private TodoService todoService;

    @GetMapping("/todos")
    public List<Todos> getAllTodos(@RequestParam(required = false) String title) {
        return todoService.getAllTodos(title);
    }

    @GetMapping("/todos/{id}")
    public Todos getTodoById(@PathVariable("id") long id) {
        return todoService.getTodoById(id)
            .orElseThrow(() -> new NoSuchElementException("Todo not found with id: " + id));
    }

    @PostMapping("/todos")
    public Todos createTodo(@RequestBody Todos todo) {
        validateTodo(todo);
        return todoService.createTodo(todo);
    }

    @PutMapping("/todos/{id}")
    public Todos updateTodo(@PathVariable("id") long id, @RequestBody Todos todo) {
        validateTodo(todo);
        return todoService.updateTodo(id, todo)
            .orElseThrow(() -> new NoSuchElementException("Todo not found with id: " + id));
    }

    @DeleteMapping("/todos/{id}")
    public void deleteTodo(@PathVariable("id") long id) {
        todoService.deleteTodo(id);
    }

    @DeleteMapping("/todos")
    public void deleteAllTodos() {
        todoService.deleteAllTodos();
    }

    private void validateTodo(Todos todo) {
        if (todo.getTitle() == null || todo.getTitle().isEmpty()) {
            throw new IllegalArgumentException("Title cannot be null or empty");
        }
        // You can add more validations here based on your requirements
    }
}