import http from "../http-common";

class TodoDataService {
    getAllTodos() {
      return http.get("/todos")
        .then(response => response.data)
        .catch(error => {
          console.error('There was an error!', error);
        });
    }
  
    getTodos(id) {
      return http.get(`/todos/${id}`)
        .then(response => response.data)
        .catch(error => {
          console.error('There was an error!', error);
        });
    }
  
    createTodos(data) {
      return http.post("/todos", data)
        .then(response => response.data)
        .catch(error => {
          console.error('There was an error!', error);
        });
    }
  
    updateTodos(id, data) {
      return http.put(`/todos/${id}`, data)
        .then(response => response.data)
        .catch(error => {
          console.error('There was an error!', error);
        });
    }
  
    deleteTodos(id) {
      return http.delete(`/todos/${id}`)
        .then(response => response.data)
        .catch(error => {
          console.error('There was an error!', error);
        });
    }
  
    deleteAllTodos() {
      return http.delete(`/todos`)
        .then(response => response.data)
        .catch(error => {
          console.error('There was an error!', error);
        });
    }
  
    findByTitle(title) {
      return http.get(`/todos?title=${title}`)
        .then(response => response.data)
        .catch(error => {
          console.error('There was an error!', error);
        });
    }
  }
  

  const todoDataService = new TodoDataService();

  export default todoDataService;