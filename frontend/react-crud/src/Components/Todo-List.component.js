import React, { Component } from "react";
import TodoDataService from "../services/Todos.service";

import "../Styles/TodoList.css";

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],

      editingTodo: null,
      showModal: false,
      editedTitle: "",
      editedDescription: "",
    };

    this.deleteTodo = this.deleteTodo.bind(this);
    this.editTodo = this.editTodo.bind(this);
  }

  componentDidMount() {
    TodoDataService.getAllTodos()
      .then((response) => {
        if (response.length > 0) {
          this.setState({ todos: response });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }

  deleteTodo(id) {
    TodoDataService.deleteTodos(id)
      .then(() => {
        // Aktualisieren des Zustands nach dem Löschen
        this.setState((prevState) => ({
          todos: prevState.todos.filter((todo) => todo.id !== id),
        }));
      })
      .catch((error) => {
        console.error("Fehler beim Löschen des Todos:", error);
      });
  }

  editTodo(id) {
    console.log(id);
  }

  openEditModal(todo) {
    this.setState({
      editingTodo: todo,
      showModal: true,
      editedTitle: todo.title,
      editedDescription: todo.description,
    });
  }

  closeEditModal() {
    this.setState({
      editingTodo: null,
      showModal: false,
      editedTitle: "",
      editedDescription: "",
    });
  }

  handleInputChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
  }

  saveTodo() {
    const { editingTodo, editedTitle, editedDescription } = this.state;
  
    const updatedTodo = {
      ...editingTodo,
      title: editedTitle,
      description: editedDescription,
    };
  
    TodoDataService.updateTodos(editingTodo.id, updatedTodo)
      .then((response) => {
        console.log("Todo erfolgreich aktualisiert:", response);
        this.reloadData(); // Daten neu laden
        this.closeEditModal();
      })
      .catch((error) => {
        console.error("Fehler beim Aktualisieren des Todos:", error);
      });
  }

  reloadData() {
    TodoDataService.getAllTodos()
      .then((response) => {
        this.setState({ todos: response });
        console.log("Daten erfolgreich neu geladen:", response);
      })
      .catch((error) => {
        console.error("Fehler beim Neu Laden der Daten:", error);
      });
  }

  

  render() {
    const { todos, showModal, editedTitle, editedDescription } = this.state;
    // Aufteilen des todos-Arrays in Gruppen von drei Elementen
    const groups = todos.reduce((acc, todo, index) => {
      const groupIndex = Math.floor(index / 3);
      if (!acc[groupIndex]) {
        acc[groupIndex] = [];
      }
      acc[groupIndex].push(todo);
      return acc;
    }, []);
  
    return (
      <div>
        <h1 className="todolist-h1">Deine Todos</h1>
        <div className="todo-container">
          {todos.map((todo) => (
            <div className="todo-block" key={todo.id}>
              <h2>{todo.title}</h2>
              <p>{todo.description}</p>
              <button 
                className="todoList-bearbeitenButton"
                onClick={() => this.openEditModal(todo)}>
                Bearbeiten
              </button>
              <button
                className="todoList-deleteButton"
                onClick={() => this.deleteTodo(todo.id)}
              >
                Löschen
              </button>
            </div>
          ))}
          {showModal && (
            <div className="overlay">
              <div className="edit-modal">
                <p>Titel</p>
                <input
                  type="text"
                  name="editedTitle"
                  value={editedTitle}
                  onChange={(e) => this.handleInputChange(e)}
                />
                <p>Beschreibung</p>
                <textarea
                  name="editedDescription"
                  value={editedDescription}
                  onChange={(e) => this.handleInputChange(e)}
                />
                <button className="todoList-bearbeitenButton" onClick={() => this.saveTodo()}>Speichern</button>
                <button className="todoList-deleteButton" onClick={() => this.closeEditModal()}>Abbrechen</button>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default TodoList;
