import React, { Component } from "react";
import TodoDataService from "../services/Todos.service";
import "../Styles/AddTodo.css";

class AddTodo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { title, description } = this.state;
    TodoDataService.createTodos({ title, description })
      .then((createdTodo) => {
        console.log("Todo successfully created:", createdTodo);
        // Leite zur Seite "/todos" zurück
        // Setze die Eingabefelder zurück
        this.setState({
          title: "",
          description: "",
        });
      })
      .catch((error) => {
        console.error("There was an error creating the todo:", error);
      });
  }

  render() {
    const { title, description } = this.state;
    return (
      <div className="add-todo-overlay">
        <div className="add-todo-container">
          <h2>Add Todo</h2>
          <form onSubmit={this.handleSubmit}>
            <label htmlFor="title">Title:</label>
            <input
              type="text"
              id="title"
              name="title"
              value={title}
              onChange={this.handleInputChange}
            />
            <label htmlFor="description">Description:</label>
            <textarea
              id="description"
              name="description"
              value={description}
              onChange={this.handleInputChange}
            ></textarea>
            <button type="submit">Add</button>
          </form>
        </div>
      </div>
    );
  }
}

export default AddTodo;
