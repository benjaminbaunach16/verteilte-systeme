import React, { Component } from "react";
import { Link, Routes, Route } from "react-router-dom"; // Importiere die Link-, Routes- und Route-Komponenten

import TodoList from "./Components/Todo-List.component";
import AddTodo from "./Components/add-Todo.component";
import Todo from "./Components/Todos.component";
import "./Styles/App.css";

import "bootstrap/dist/css/bootstrap.min.css";

class App extends Component {
  render() {
    return (
      <div className="navbar-container">
        <nav className="navbar">
          <a href="/todos" className="navbar-logo">
            Benjamin Baunach
          </a>
          <div className="navbar-item-container">
            <li className="nav-item">
              <Link to={"/todos"} className="nav-link">
                Todos
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/add"} className="nav-link">
                AddTodo
              </Link>
            </li>
          </div>
        </nav>

        <div className="content-container">
          <Routes>
            <Route path="/" element={<TodoList />} />
            <Route path="/todos" element={<TodoList />} />
            <Route path="/add" element={<AddTodo />} />
            <Route path="/tutorials/:id" element={<Todo />} />
          </Routes>
        </div>
      </div>
    );
  }
}

export default App;
